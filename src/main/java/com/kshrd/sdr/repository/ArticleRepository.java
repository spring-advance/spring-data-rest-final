package com.kshrd.sdr.repository;

import com.kshrd.sdr.repository.model.Article;
import io.swagger.annotations.Api;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.transaction.Transactional;
import java.util.Optional;

@Api(tags = "Article Repository")
@RepositoryRestResource
@Transactional
public interface ArticleRepository extends JpaRepository<Article, Integer> {
    @Override
    @Secured("ROLE_ADMIN")
    Page<Article> findAll(Pageable pageable);

    @Override
    @PostAuthorize("returnObject.get().getAuthor() == authentication.name")
    Optional<Article> findById(Integer integer);

    @Override
    @PreAuthorize("#article.getAuthor() == authentication.name")
    void delete(Article article);
}
