package com.kshrd.sdr.repository.projection;

import com.kshrd.sdr.repository.model.Article;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "noCategory", types = {Article.class})
public interface OmitCategoryData {
    @Value("#{target.id}")
    long getId();
//    @Value("#{target.title} #{target.description}")
//    String getTitleAndDescription();
    String getTitle();
    String getAuthor();
    String getDescription();
}
