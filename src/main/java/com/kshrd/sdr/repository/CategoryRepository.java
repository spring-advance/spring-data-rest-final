package com.kshrd.sdr.repository;

import com.kshrd.sdr.repository.model.Category;
import io.swagger.annotations.Api;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Api(tags = "Article Repository")
public interface CategoryRepository extends JpaRepository<Category, Integer> {
}
