package com.kshrd.sdr.configuration;

import com.kshrd.sdr.repository.model.Article;
import com.kshrd.sdr.repository.model.Category;
import com.kshrd.sdr.repository.projection.OmitCategoryData;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class SpringDataRestConfiguration implements RepositoryRestConfigurer {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.setBasePath("/api/v1");
        config.exposeIdsFor(Article.class, Category.class);
        config.getProjectionConfiguration().addProjection(OmitCategoryData.class);
    }
}
